# Obscura CMS


## Description
Obscura Content Management System, in it's shortened form also called Obscura or Obscura CMS, is being built almost from scratch - let's name it semi-scratch.

Why semi-scratch? Well we use various tools, methods & examples found on the web. A major portion of the framework code got it's start from the [PatrickLouys/no-framework-tutorial](https://github.com/PatrickLouys/no-framework-tutorial) found on [GitHub](https://github.com)

<!--
## Badges


## Visuals


## Installation


## Usage
-->

## Documentation
A majority of our documentation will be done on the [project's website](https://obscura-cms.com). During development you may find some documentation in the [project's wiki pages](../../wikis/home).

## Support
Support, Issues, Bug Reports, Suggestions and more can be found in our [ISSUES AREA](../../issues). At a laater point in Development we will be adding a [SUPPORT FORUM](https://forum.obscura-cms.com) on the [project's website](https://obscura-cms.com).

## Roadmap
To see where development is heading, all you need to do is follow up with our [Milestones](../../milestones).

<!--
## Contributing


## Authors and acknowledgment
-->

## License
[MIT License](LICENSE)

## Project status
Active Development. No releases yet.
